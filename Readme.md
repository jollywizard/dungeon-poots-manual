# Dungeon Poots Manual

## Introduction

Dungeon Poots is a tabletop game which blends the self propelled structure and balance of traditional card games with the dice oriented combat and stat based character status of tabletop RPGs.

As a card game, Dungeon Poots plays similar to Rummy, where each player takes turns attempting to acquire and combine cards for rewards.

As an RPG, Dungeon Poots features dice based combat, a sword and sorcery duality, character progression, status effects, inventory items, crafting, and randomized events.

Combining both sides is the concept that all character stats and game events are based on card suits, as represented in the tradition of the Tarot.  The game is intended as a tribute to this lucid tradition, as well as an introduction to some of its essential paradigms.

Ultimately, Dungeon Poots is designed to be a framework to develop RPGs that are self propelled, easy to play, easy to learn, and extensible.  

To solve these problems compared to traditional RPGs, Dungeon Poots:

* Does not depend on the planning, effort, and personality of a game leader.
* Separates storytelling from game rules.
* Rewards bonus dice instead of bonus modifiers.
* Uses out of turn players to control monsters.
* Limits the number of character stats to 4 (HP, MP, Att, & Def).
* Combines experience, items, and money into a single resource (cards).

By design, Dungeon Poots hopes to invert the cycle. Instead of using imagination to drive player involvement, it aims to use player involvement to drive imagination.

Enjoy!

