#!/bin/bash

source src/bash/setup-env.sh

diaout=$output/images
mkdir $diaout

for f in $dia/*.dia
do
  echo $f
  file=${f##*/}
  base=${file%.*}
#  echo $file
#  echo $base

  dia -n -t svg -e "$diaout/$base.svg" $f
done
