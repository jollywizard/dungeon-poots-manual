#!/bin/bash

source src/bash/setup-env.sh

pandoc -r markdown_github -w html5 -s --toc $markdown/*.md > $output/compiled.html

