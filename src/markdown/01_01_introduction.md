## Introduction

:DP: (Dungeon Poots) is a tabletop game which blends the self propelled structure and balance of traditional card games with the variety and action of tabletop RPGs.

As a card game, :DP: plays similar to Rummy, where each player takes turns attempting to acquire and combine cards for rewards.

As an RPG, :DP: features dice based combat, a sword and sorcery duality, character progression, status effects, inventory items, crafting, and randomized events.

May you experience the best of both worlds.

 \- James Arlow
