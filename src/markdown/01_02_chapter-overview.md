## Chapter Descriptions 

### Overview

:DP: is one of those things that is really simple, but complicated to explain.

To prevent having to explain each rule every time it is needed, each chapter of this manual covers a different part of the game. 

Chapters 2-4 explain the main concepts and structure of the game.

Chapters 5-8 explain special rules that apply to each section of the game.

### Breakdown

**Chapter 1 | The Preface**

See that star on the map. You are here. I really don't know what else to tell you about this. It's not part of the rules, per se. It is what it is.

**Chapter 2 | Core Concepts**

An overview of the essential concepts required to use the manual and understand the game.

**Chapter 3 | Main Phases**

The rules that describe the main structure of the game, from beginning to end.

**Chapter 4 | Turn Phases**

The rules that describe the main parts of each turn.

**Chapter 5 | Event Phases**

The rules that describe each type of random event.

**Chapter 6 | Combat Phases**

The rules that describe how a combat event is structured.

**Chapter 7 | Duel Phases**

The rules that describe the actual fighting portion a of combat event.

**Chapter 8 | Limit Phases**

The rules that describe the end of each turn.

**Appendices**

Special rules and insights into the design of the game.