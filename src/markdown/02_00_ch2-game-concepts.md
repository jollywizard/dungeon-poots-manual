# Chapter 2 | Core Concepts

## Overview

In this chapter, we'll cover the core concepts required to use this manual and play the game.

## Section Breakdown

**Rules**

Here we go over the types of rules that are used in the manual.

**Player Goals**

Here we go over the main goals of the game.

**Stats**

Here we go over how numbers are used and tracked in the manual and in the game.

**Melds**

Here we go over how cards combinations are used in the game.