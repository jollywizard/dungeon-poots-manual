# Rules

:DP: is designed to be a framework.  It provides a set of rules to help play a game have fun doing it.

## Rules

A **Rule** is an instruction that explains how to play a part of the game. 

Rules are defined in terms the key game concepts described in this chapter.

## House Rules

A **House Rule** is a rule that players decide on. 

Not all rules are fun. The official rules for another game insist that there is no free parking. They go so far as to put a rule in the manual that it is not a rule, because so many people decided it should be. Talk about Captain Buzzkill. :DP: mutineers against such non-sense; the official rule is that players decide the rules.

House Rules can be as crazy or as simple as players want them to be. They can be based on the tools already in the game, or they can bring new elements to the table. 

If the house rules say you have to get kicked in the genitals, brace yourself. If they say the loser has to go to the store and get snacks, tough luck, Buttercup. If these rules sound terrible, don't use them. The players decide which rules to play by.

House rules are not only encouraged, they are actually required by other rules. 

For example, there is no fun in being told how you must pick the first player of the game. :DP: makes a suggestion, to help you play, and that is it.

## Default Rules

A **Default Rule** is a suggestion for a house rule that becomes the rule when players can't agree.

By name, a house rule is decided by the owner of the house, but in general, all players must agree to follow them. If players can't agree on a house rule, when required, the default rule is one to be used.

## Actions

An **Action** is a rule that describes a player activity.

Actions can include:

* Drawing cards
* Rolling Dice
* Spending Points
* ...

## Exceptions

An **Exception** is a rule that lies in wait and interrupts other rules.

When it is time for the exception to happen, any other rules are paused and the exception action takes place.

The **Trigger** is the condition which can causes an exception to take place.

The **Result** is the action that takes place when the exception is triggered.

## Phases

Actions come in groups. A group of actions can be a rule.

For example, each turn, the player draws a card, plays an event, and then discards. One rule, take a turn, made of three parts. Each part must be played in order.

A **Phase** is a group of actions that must played in order. 

The manual explains :DP: in terms of phases. Each phase of the game is defined in its own section.

### Parent and Child Phases

The entire game is called the **Main Phase**.  It is composed of three phases, each composed of more.  

A **Parent Phase** is a phase that contains other phases.

A **Child Phase** is a phase that takes place during another phase.

Gameplay returns to the parent phase (at the next action), when the child phase is complete.

There are no dead ends. Every phase leads to another phase (or back) until the game is over. Each phase will explain in the rules, when to begin another phase and what to do when the phase is complete.

### Cycles

A **Cycle** is a phase that repeats over and over until it is finished.

The last action of a cycle is followed by the first action of the cycle.

Each loop of the cycle is referred to as a **Turn**.
