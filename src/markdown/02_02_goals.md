# Player Goals

What is the point of the game?  Good question.

## Adventuring

The goal of Dungeon Poots is simple: Level Up.

To achieve this goal, players must adventure.  

* Get Juiced
* Fight Monsters
* Collect Cards.

Adventuring is designed to help you level. The higher your level, the higher your chance of randomly winning the game.

## Winning the Game

There are two ways to win Dungeon Poots:

### Breaker Breaker

Any time a player draws from the deck, if there are not enough cards to complete the draw, the game is broken and ends immediately. 

The player whose draw broke the game is the winner.

*This is why it is important to Level Up. The higher the player's level, the more cards they draw from the deck.*

*The game is designed so this is most likely when entering combat, aka fighting an impossible monster.* 

### Last Man Standing

If you are the only player left.  You win!

*Players play the monsters, too. If there is only one player, there is nobody to fight.*

