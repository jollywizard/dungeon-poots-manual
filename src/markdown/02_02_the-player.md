# The Player

## Characters

Over the course of the game, each player will take on the roles of different characters.

## Types of Characters

* Heroes
* Monsters

### Heroes

Each player is assigned a hero.

The player controls this hero during their turn.

### Monsters

Monsters are randomly generated during **Combat** events.

When it is not their turn, players control monsters.
