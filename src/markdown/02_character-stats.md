# Numbers and Stats

Numbers are the rockbed of game decisions.  Questions like `who did better?`, `how many dice?`, and `what do I need?` are all answered with numbers.

## Stats

All characters, heroes and monsters, have a set of **Stats**, that are used to decide other numbers.  

Stats are based on the four suits of the deck and their traditional affinities.

<pre>
 Stat  | Suit       | Tarot  | Alchemy 
---------------------------------------
 Magic | &spades; Spades   | Wands  | Fire
 Gusto | &hearts; Hearts   | Cups   | Water
 Skill | &clubs; Clubs    | Swords | Air
 Form  | &diams; Diamonds | Disks  | Earth
</pre>

### Stat Levels

Stats are measured in levels. 

All stats start at the minimum level, 1.

### Buffs

Buffs are temporary increases to a stat.

When using the stat, the 

		[Stat] = [Stat] :: [Level] + [Stat] :: [Buff]

Buffs are lost when a player dies.

### Hero Stats

Each player starts the game with a Level 1.

#### Hero Level

The **Hero Level** is used to decide information.

		[Hero] :: [Level] = min([Hero] :: [Stats])

#### Hero Rank

#### Example

<pre>
 &spades; | &hearts; | &clubs; | &diams; || L | R
------------------------
 2 | 4 | 3 | 2 || 2 | 11  
 3 | 4 | 3 | 2 || 2 | 12
 3 | 4 | 3 | 3 || 3 | 13
</pre>


### Monster Stats

Monster stats work the same as hero stats during combat.

At the beginning of combat, the monster is created.

Players draw cards equal to the monsters level.

When a monster is created, it gains one stat level for each related card in the monster stash.

## Energy 

Each player has two types of energy: 

* Health
* Mana

Unlike stats, which can only increase, energy increases and decreases frequently.

### Health

Health represents how alive the character is.

A character with 0 health is dead.