# Chapter 3 | The Main Phase

## Overview

In this chapter we'll cover the **Main Phase** and the sections that compose it.

The main phase represents the structure of a complete game.

## Main Phase Order

* Game Setup
* Turn Cycle
* Game Over

## Main Phase Summary

The game begins at **Game Setup**.  During this phase, players are created, decks are built, and feelies are readied.

Once setup is complete, the game enters the **Turn Cycle**.  During this phase, players adventure, getting juiced, battling monsters, and collecting treasure. The Turn Cycle repeats until the game is won.

When the game is won, the **Game Over** is reached.  By default, the winner is declared here, and everybody must pretend to be super happy for them. House rules may introduce special conditions, such as New Game+ or 2 out of 3, that lead back to another different phase.

<div><img src="images/main-phase_horizontal.svg" style="max-width:100%;" /></div>
