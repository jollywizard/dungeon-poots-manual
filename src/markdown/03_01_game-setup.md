# 3.1 | Main Phase | Game Setup

## Overview

Before the adventuring can begin, the supplies need to be collected and the heroes readied.

## Phase Order

* Decide on the Rules
* Ready the Feelies
* Build the Deck
* Birth the Heroes

## Phase Breakdown

### Decide on the Rules

Decide which house rules will be included during the game. 

By default, house rules can only be added at the beginning of the game.

### Ready the Feelies

Feelies are things you can touch with your hands.

To play :DP:, four types of feelies are needed:

* Cards 
* Dice
* Pen and Paper
* Tokens

Begin the game by collecting all of the feelies in the game space.

#### Cards

:DP: is played with regular playing cards.

Any number of cards can be used.  Players decide at the beginning of the game how many cards will be used.

For a default game of 2-4 players, 2 standard decks are recommended.  

#### Dice

:DP: is played with regular six-sided dice (aka D6).

Players can roll anywhere from 1-30+ dice at a time.  

When setting up the game, it is recommended to have as many as possible available in a place where all players can reach them.

#### Pen and Paper

:DP: is played with character sheets.  Each player should get a sheet of paper and a writing utensil.

For new players, pre-made instruction and characters sheets are encouraged.  Decide which paper materials will be used and have them ready at the beginning of the game.

#### Tokens

While optional, :DP: encourages the use of tokens to track information that changes frequently. 

Any type of marker can be used.  Suggestions include:

* Poker Chips
* Coins

It is recommended that a different type of token is used for each type of information being tracked.  

* Health Points
* Mana Points

Player power levels must be tracked between turns and during combat.  It is highly recommend that all players track these levels with tokens.

* Combat Advantage

During combat, players have a temporary stat that represents their advantage over their opponent.  It is recommended that new players track advantage with tokens.  Veteran players may prefer to use their heads.

### Build the Deck

Take all of the cards, and put them into a single deck.  

This is the game deck.  

Shuffle it, a lot.

### Birth the Heroes

Each player in :DP: adopts the role of a hero character, who they level up over the course of the game.

To begin the game, each player sets up their hero.

#### Bestowing Life

In the beginning, there were no stats. Stats represent the essence of the character.

The character has been bestowed essence. Mark each stat Level 1.

#### The Christening

Lest the hero be nameless, here now, they shall be named.

## Afterwards

The game is now ready to play. Proceed to the Main Cycle phase.