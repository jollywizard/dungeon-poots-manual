# 3.2 | Main Phase | Turn Cycle

## Overview

Once setup is complete, the game enters the main cycle.

Each loop of of the cycle is a Turn.

Each turn is centered around an event, which offers a player a chance for reward. 

The turn is played out in 3 phases. See *Chapter 4 | Turn Phases* for a breakdown of each phase.

## Exceptions

Once the turn cycle begins, the rules for winning the game are in effect.

* **Breaker Breaker**
* **Last Man Standing**