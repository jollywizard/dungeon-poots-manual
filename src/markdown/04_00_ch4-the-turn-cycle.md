# Chapter 4 | Turn Phases

## Overview

In this chapter, we'll cover the phases and actions that make up each turn of the **Turn Cycle**.

Remember: **Breaker Breaker** and **Last Man Standing** are in effect during the **Main Cycle**. At any point, if these rules are triggered, the **Main Cycle** is over and the game proceeds to the **Game Over** phase.

## Turn Phase Summary

During each turn players experience a random event.

First, the player is chosen and a card is drawn to **Determine** the event type.

To **Resolve** the event, the game proceeds to the phase associated with the event type.

Finally, the player must level up or discard until they are under the inventory **Limit**.

<div><img src="images/main-cycle_horizontal.svg" style="max-width:100%;" /></div>

## Turn Phase Order

* Determine
* Resolve
* Limit

