# 4.1 | Turn Phase | Determine

## Overview

During the determine phase, decisions are made that will shape the rest of the turn.

## Phase Order

* Select Player
* Revive Player
* Reload Inventory
* Draw Event

## Phase Breakdown

### Select Player

The player is selected according to house rules. 

* By default, players rotate clockwise. 
* The first player can be decided by a dice roll.

### Revive Player

If the player is currently dead, the player is revived.  

		Dead Player => Alive Player
		[Health] = [Gusto]

### Reload Inventory

If the player is low on inventory, they can draw from the deck.

		if ([Hand Size] < [Level]) 
		draw([Level] - [Hand Size])

### Draw Event

Each event is decided by a card.  When the player is ready to begin the next phase, the event card is drawn.

		[Event Card] = draw(1)
		end_phase()

## Afterwards

Proceed to the Resolve phase.