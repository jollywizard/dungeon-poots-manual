# 4.2 | Game Phase | Resolve

## Overview

During the resolve phase, the event is played out as a game of chance or skill.

## Phase Order

* Decide Event
* Follow Event

## Phase Breakdown

### Decide Event

The event phase is decided based on the type of the event card.

		[Event Phase] = event_lookup([Event Card])

#### Event Lookup Table

<pre>
 Event Card  | Event Phase
---------------------------
 Spades      | Charge
 Hearts      | Heal
 Clubs       | Combat
 Diamonds    | Treasure 
</pre>


### Follow Event

Once the Event Phase is decided, it is played out to completion.

		do_phase([Event Phase])

## Afterwards

Proceed to the Limit Phase