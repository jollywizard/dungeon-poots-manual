# 4.3 | Game Phase | Limit

## Overview

During the limit phase, the player has the opportunity to level up.  After spending cards, the player chooses which cards to keep or discard.

## Phase Order

* Level Up
* Discard

## Phase Breakdown

### Level Up

All players now have the opportunity to level up.

		do_phase(<Level Up>)

### Discard

If the player's hand size is greater than their inventory limit, the player must discard until under the limit.

		do_phase(<Discard>)

## Afterwards

The player's turn is complete.

Proceed to the Determine phase to begin a new turn.