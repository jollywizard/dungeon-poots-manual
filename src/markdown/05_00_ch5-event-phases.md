# Chapter 5 | Event Phases

## Overview

In this chapter, we'll cover the phases and actions that compose each of the four events that can occur during the **Main Cycle**.

