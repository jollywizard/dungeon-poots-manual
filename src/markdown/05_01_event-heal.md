# 5.1 | Event Phase | Heal

## Overview

During a heal phase, the player has an opportunity to recover lost health points.

## Phase Order

* Base Dice
* Bonus Dice
* Roll Dice
* Convert Points

## Phase Breakdown

### Base Dice

For a healing roll, the player uses the Gusto stat.

		[Heal Dice] += [Gusto]

### Bonus Dice

The player may use **Dice Boost** for more dice.

		[Heal Dice] += dice_boost()

### Roll Dice

Once the player has all of their dice, they roll them.

		[Heal Points] = roll([Heal Dice])

### Convert Points

All of the heal points are added to the players health.

		[Health] += [Heal Points]
		[Heal Points] = 0

## Afterwards

Proceed to the next step of the parent phase.

---
