# 5.2 | Event Phase | Charge

## Overview

During a Charge phase, the player has an opportunity to recover lost mana points.

## Phase Order

* Base Dice
* Bonus Dice
* Roll Dice
* Convert Points

## Phase Breakdown

### Base Dice

For a healing roll, the player uses the Mana stat.

		[Chage Dice] += [Mana]

### Bonus Dice

The player may use **Dice Boost** for more dice.

		[Heal Dice] += dice_boost()

### Roll Dice

Once the player has all of their dice, they roll them.

		[Charge Points] = roll([Charge Dice])

### Convert Points

All of the charge points are added to the players Mana.

		[Mana] += [Charge Points]
		[Charge Points] = 0

## Afterwards

Proceed to the next step of the parent phase.