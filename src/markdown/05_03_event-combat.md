# 5.3 | Event Phase | Combat

## Overview

During a combat event, a random monster is encountered and battled. Each monster comes with a stash of cards, awarded to the winner of the battle as loot.

## Phase Order

* Encounter
* Possess
* Duel
* Loot

## Phase Breakdown

### Encounter

Before a monster can be fought, it must be created. Encountering the creature discovers what kind it will be.

		do_phase(<Encounter>)

### Possess

During the duel, the monster will be played by another player, aka the Monster Player. 

House rules decide who this player will be.

By default, use the player whose turn is next.

### Duel

Once the monster is created and the Monster Player is selected, the battle can begin.

		do_phase(<Duel>)

*See: Chapter 6 | Combat Phases*

### Loot

When the battle is over, the winner collects the monster's stash.  

		[Winning Player].[Stash] += [Monster Stash]

## Afterward

Proceed to the next step of the parent phase.