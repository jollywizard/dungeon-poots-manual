# 5.4 | Event Phase | Treasure

## Overview

During a treasure phase, the player gets to draw cards from the deck.  

## Phase Order

* Draw Items
* Add to Hand

## Phase Breakdown

### Draw Items

House rules decide how many cards to draw.

* By default, use the player's level.

		[Treasure Cards] = draw([Player Level])

### Add to Hand

The player gets to keep all of the treasure cards.

		[Player Hand] += [Treasure Cards]

## Afterwards

Proceed to the next step of the parent phase.