# Chapter 6 | Combat Phases

## Overview

In this chapter, we'll cover the actions and phases that make up the **Combat** event phase.

A combat event is more complex than a simple fight. Each battle takes place against a randomly generated monster that must be created. Afterwards, the winner collects treasure.

As we'll discuss further on, the actual details of battling are covered in **Chapter 7 | Duel Phases**.

## Combat Phase Summary

First, a monster **Encounter** takes place. The strength of the monster is decided, the cards for the monster are drawn from the deck, and a player is chosen to control the monster.

Next, the **Duel** begins. The hero and the monster fight each other until one of them is dead.

Finally, the **Loot** from the monster stash is awarded to the winner.

<div><img src="images/combat_horizontal.svg" style="max-width:100%;" /></div>