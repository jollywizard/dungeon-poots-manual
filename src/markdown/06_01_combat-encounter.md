# 6.1 | Combat Phase | Encounter

## Overview

A combat event represents an encounter with a random monster. Monsters are played by another player. 

During the **Encounter** phase, the monster is created and its player is selected.

## Phase Order

* Select Foe
* Set Difficulty
* Draw Monster

## Phase Breakdown

### Select Foe

Select which player will control the monster. House rules decide. 

By default, use the next player in rotation.

### Set Rank

The **Monster Rank**, decides how the monster is created. House rules decide.

By default, choose one of three difficulty levels at the beginning of the game.

		easy:   [Monster Rank] = [Player Level] 
		normal: [Monster Rank] = [Player Rank] + [Player Level]
		hard:   [Monster Rank] = [Player Rank] * [Player Level] 

### Draw Monster

Use the **Monster Rank** to draw a monster from the deck.

Draw the cards and add them to the monster stash.

		[Monster Stash] = draw([Monster Rank])

### Ready Stats

Use the cards in the stash to setup the monster's stats.

		[Gusto] = 1 + Stash<Hearts>
		[Mana]  = 1 + Stash<Spades>
		[Skill] = 1 + Stash<Clubs>
		[Form]  = 1 + Stash<Diamonds>


---
