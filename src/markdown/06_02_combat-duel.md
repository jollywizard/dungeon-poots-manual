# 6.2 | Combat Phase | Duel

## Overview

Once the monster is ready, the actual battle between the hero and the monster takes place.

---
