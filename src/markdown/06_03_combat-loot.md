# 6.3 | Combat Phase | Loot

## Overview

Once the battle is complete, collect the reward cards.

## Phase Order

* Collect Rewards

## Phase Breakdown

### Collect Rewards

The winning player adds all of the cards from the **Monster Stash** to their hand.

		[Winning Player Hand] += [Monster Stash]

---
