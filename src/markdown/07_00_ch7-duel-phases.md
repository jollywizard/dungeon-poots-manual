# Chapter 7 | Duel Phases

## Overview

In this chapter, we'll cover the actions and phases that make up the **Duel** phase. 

The duel is a cycle that takes place during **Combat** events.  It is the part of the combat phase where the actual battling takes place.

## Duel Phase Summary

Each turn, both players have a chance to get **Ready** by using items and magic. Then, the players try to **Strike** each other. If a player is hit, he will have the chance to defend and counter with a **Parry**.

<div><img src="images/duel_horizontal.svg" style="max-width:100%;" /></div>