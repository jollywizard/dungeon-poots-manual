# 7.1 | Duel Phase | Ready

## Overview

In the ready phase, both players have a chance to recover before the next strike.

## Phase Order

* Combat Boost

## Phase Breakdown

### Combat Boost

Each player may choose one boost option:

Either:

* 1 Potion
* 1 Recovery Boost

---
