# 7.2 | Duel Phase | Strike

## Overview 

Each turn of the duel, both players try to strike each other. The player who does better gains an offensive advantage.

## Phase Order

* Ready Dice
* Boost Dice
* Roll Dice
* Resolve Advantage
* Land Strikes

### Phase Breakdown

#### Ready Dice

For the Strike phase, the attack stat is used for dice.

<pre>
    [Strike Dice] += [&#x2663; Skill]
</pre>

#### Boost Dice

The defense may use **Dice Boost** for more dice.

		[Strike Dice] += dice_boost()

#### Roll Dice

Both players roll to establish their advantage.

		[Both Players] :: [Advantage] = roll([Strike Dice])

#### Resolve Advantage

The advantages cancel out, until one of the players is at 0.

		while( [Both Players] :: [Advantage] > 0 )
			[Both Players] :: [Advantage] -= 1

A player who ends the strike with advantage is the **Offense**.

A player who ends the strike at 0 is the **Defense**.

		if ( [Advantage] > 0 )
          [Player] = [Offense]
		else 
          [Player] = [Defense]

#### Land Hits

The offense has the option to spend their advantage to hit the defense.

		[Defense] :: [Hits] = [Offense] :: spend([Advantage])

## Afterwards

Proceed to the **Parry** phase.

---
