# 7.3 | Duel Phase | Parry

## Overview

When a player is on defense, they must block any strikes against them or they take damage.

If the player defends well, they may have an advantage for the next strike.

## Phase Order

* Ready Dice
* Boost Dice
* Roll Dice
* Resolve Damage

### Phase Breakdown

#### Ready Dice

For the Parry phase, the defense stat is used for dice.

<pre>
    [Parry Dice] += [&#x2662; Form]
</pre>

#### Boost Dice

The defense may use **Dice Boost** for more dice.

		[Parry Dice] += dice_boost()

#### Roll Dice

The defensive player rolls their defense dice.

		[Advantage] += roll([Parry Dice])

#### Resolve Damage

The defensive advantage cancels out incoming strikes.

What strikes remain become damage.

Extra advantage is carried over to the next turn.

<pre>
		[Defense] :: [Advantage] -= [Strikes]
		if ([Defense] :: [Advantage] < 0)
				[Defense] :: [Health] += [Advantage]
				[Defense] :: [Advantage = 0]
</pre>

---
