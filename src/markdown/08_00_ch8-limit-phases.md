# Chapter 8 | Limit Phases

## Overview

In this chapter, we'll cover the actions and phases that compose the **Limit** phase, the final phase of each **Main Cycle** turn.

