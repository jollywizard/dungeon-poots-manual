# 8.2 | Limit Phase | Level Up

## Overview

During the Level Up phase, the cards players collect over the course of the game can be spent to upgrade the players core stats.

## Phase Order

* Calculate Cost
* Build Meld
* Spend Upgrade

## Phase Breakdown

### Calculate Cost

The cost to upgrade a stat depends on the current level for that stat.

		for each (Stat) [Stat Cost] = [Stat Level]

### Build Meld

If the player wants to upgrade a stat, they need to build a Meld to pay the cost.

* The Meld Type must be SF([Stat Suit]) or XK
* The Meld Size must be equal to [Stat Level]

			[Valid Melds] = 
			select from Melds, Stats
			where [Meld Size] = [Stat Cost]
			  and [Meld Type] = XK
			   or [Meld Type] = SF([Stat Suit])

### Spend Upgrade

If the player has a valid meld for a stat, they can spend the meld to upgrade the stat.

* Discard the meld.
* Increase the stat by 1.

## Afterwards

Proceed to the next stage of the parent phase.