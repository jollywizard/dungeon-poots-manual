# Chapter 9 | Boosts

## Overview

In this chapter, we will cover the rules for each type of Boost.

Boosts are options players have throughout the game to spend stuff on temporary advantages.

## Ways to Boosts

Boosts cost juice.

Each suit represents a different juice.

Juice can come in a potion, or from spending energy.

<pre>
 Suit     | Juice  | Potion | Energy
----------------------------------------
 Hearts   | Blood  | Health | Health
 Spades   | Magic  | Mana   | Mana
 Clubs    | Sweat  | Skill  | Advantage
 Diamonds | Tears  | Loot   | Items
</pre>

## When to Boost

Different boosts can be used in different situations.

* Potion Boosts are used to trigger other bonus events.
* Dice Boosts are used to add more dice to a roll.
* Recovery Boosts are used to trigger recovery events.

---
