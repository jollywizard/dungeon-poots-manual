# 9.1 | Potions

Potions are items that used to boost and get juiced.

Potions can only be used when it is safe to do so.

Each potion is filled with a different type of juice.

The type of potion depends on the suit of the cards

<pre>
 Suit     | Juice | Type 
---------------------------------
 Hearts   | Blood | Health Potion
 Spades   | Magic | Mana Potion
 Clubs    | Sweat | Skill Potion
 Diamonds | Tears | Item Potion
</pre>

---

## Health Potion

### Cost

1 Heart

### Result

The player does a heal phase.

OR

The player gains 1 Health Token.

---

## Mana Potion

### Cost

1 Spade

### Result

The player does a charge phase.

OR 

The player gains 1 Mana Token.

---

## Skill Potion

### Cost

1 Clubs

### Result

The player chooses a stat to boost.

The player rolls a single die.

If the roll is a success, the stat gets a 1 point buff.

<pre>
		[Stat] = choose_one([Stats])
     if ( roll(1) = [Success] )
        buff([Stat])
</pre>

---

## Loot Potion

### Cost

1 Diamond

### Result
	
The player rolls to decide how many items to draw.

The player picks one item to keep. The rest are discarded.

<pre>
		[Choices] = draw( roll([&#x2662;Form]) )
		[Inventory] += choose_one([Choices])
</pre>

---
