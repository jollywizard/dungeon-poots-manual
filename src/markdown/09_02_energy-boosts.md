# 9.2 | Energy Boosts

Everything has energy. That energy can be converted into juice and used to boost.

## Conversion

Any type of juice can be used for energy.

To create energy, discard or lose the cost of the juice.

<pre>
 Juice   | Cost
----------------------
 1 Blood | 1 Health
 1 Magic | 1 Mana
 1 Sweat | 1 Advantage
 1 Tears | 1 Item
</pre>

## Dice Boost

Spend *X* energy to roll *X* extra dice.

## Recovery Boost

Players can spend 1 energy to trigger a recovery phase.

### Recover Health

Spend 1 energy to trigger a Heal phase.

### Recover Mana

Spend 1 energy to trigger a Charge phase.