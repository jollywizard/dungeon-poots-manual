# Appendix | Melds

## Overview

A meld is a special combination of cards.

In Dungeon Poots, Melds have a size, a group, and a type.

## Meld Size

The number of cards in the meld.

## Meld Group

Cards are grouped by suit and by number.

The type of the group depends on the type of the meld.

All cards in a meld must belong to the same group.

## Meld Types

There are two types of Melds in Dungeon Poots.

* SF | Straight Flush | Groups by Suit
* XK | X of a Kind | Groups by Number

## SF | Straight Flush

Grouped by : Suit

* Requires every number between the lowest and highest card.

#### Examples

* SF(Spades)[3] | Spades{A,2,3}
* SF(Hearts)[3] | Hearts{7,7,8} 
* SF(Clubs)[7] | Clubs{9,10,J,Q,K,A,2}

## XK | X of a Kind

Grouped by: Number

* Cards can be of any suit.

#### Examples

* XK(3)[2] | Spades{3} , Hearts{3}
* XK(J)[4] | Clubs{J}, Hearts{J,J}, Spades{J} 

## Short Codes

For rules and examples, the following formats can be used to describe Melds.

* TYPE[Size]
* TYPE(Group)[Size]
* TYPE(Group, Size)

