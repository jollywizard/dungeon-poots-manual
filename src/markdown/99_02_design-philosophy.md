# Design Philosophy

:DP: was designed to be a card game and an RPG.

Merging the duality is the concept that all character stats and game events are based on card suits, as represented in the tradition of the Tarot. The game is intended as a tribute to this lucid tradition, as well as an introduction to some of its essential paradigms.

Ultimately, :DP: is designed to be a framework to develop RPGs that are self propelled, easy to play, easy to learn, and extensible.  Of these qualities, in the authors opinion, most RPGS only succeed on the last point.

To solve these problems compared to traditional RPGs, Dungeon Poots:

* Does not depend on the planning, effort, and personality of a game leader.
* Separates storytelling from game rules.
* Rewards bonus dice instead of bonus modifiers.
* Uses out of turn players to control monsters.
* Derives all stats from the the four suits of the playing card deck.
* Combines experience, items, and money into a single resource (cards).

By design, :DP: hopes to invert the cycle. Instead of using imagination to drive player involvement, it aims to use player involvement to drive imagination.

Enjoy!